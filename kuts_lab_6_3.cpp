/*file name: kuts_lab_6_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 5.11.2021
*дата останньої зміни 5.11.2021
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та
 реалізувати його мовою С\С++ для розв‘язання
 індивідуального завдання, що представлено у таблиці И.1 з додатку И. 
*призначення програмного файлу: У заданому масиві r5(n) усі парні елементи замінити їх індексами.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int size, i; 
    cout << "Введіть розмір масиву = ";
    cin >> size;
    int r5[size];
    printf("Введіть масив r5[%d] = ",size);
  	for (i=0;i<size;i++)
	    {
			cin>>r5[i];
		} 
	system("cls");
	printf("Вхідний масив r5[%d] = \n",size);
	for (i=0;i<size;i++)
	    {
	    	printf("%4d",r5[i]);
			if(r5[i]%2 == 0) {
				r5[i]=i;
			}
			
		} 
	printf("\n Вихідний масив r5[%d] = \n",size);
	for (i=0;i<size;i++)
	    {
			printf("%4d",r5[i]);
		}
	cout<<endl;
	system("pause");
	return 0;
}
