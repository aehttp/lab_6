/*file name: kuts_lab_6_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 5.11.2021
*дата останньої зміни 5.11.2021
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та
 реалізувати його мовою С\С++ для розв‘язання
 індивідуального завдання, що представлено у таблиці И.1 з додатку И. 
*призначення програмного файлу: Обчислення кількості певних елементів масиву. 
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int n,m, i , j,k=0 ;
	cout<<"к-сть рядків = \t";
	cin>>n;
	cout<<"к-ть стовбців =\t";
	cin>>m;
	int a[n][m];
	for(i=0 ; i<n; i++){
		for(j=0; j<m ; j++){
			a[i][j] =rand()%100-50;
			printf("%4d", a[i][j]);
			 if (a[i][j]%2 == 0 && a[i][j] >0) {
			      k++;
			 }
		}
		cout<<endl;
	} 
	cout<<endl;
	printf(" кількість = %d \n",k);
	system("pause");
	return 0;
}
