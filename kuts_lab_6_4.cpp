/*file name: kuts_lab_6_4 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 5.11.2021
*дата останньої зміни 5.11.2021
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та
 реалізувати його мовою С\С++ для розв‘язання
 індивідуального завдання, що представлено у таблиці И.1 з додатку И. 
*призначення програмного файлу: Сформувати масив c3(n) елементи якого формуються за правилом чисел Фібоначі.
*варіант : №4
*/
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, n;
	cout << "Введіть розмір масиву = ";
    cin >> n;
    int c3[n];
    c3[0]=1;
    c3[1]=1;
  	for (i=2;i<n;i++)
	    {
			c3[i]=c3[i-1]+c3[i-2];
		} 
	printf("Масив a3[%d] = \n",n);
	for (i=0;i<n;i++){
		printf("%4d",c3[i]);	
	}
	cout<<endl;
	system("pause");
	return 0;
}
