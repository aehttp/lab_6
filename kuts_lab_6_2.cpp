/*file name: kuts_lab_6_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 5.11.2021
*дата останньої зміни 5.11.2021
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та
 реалізувати його мовою С\С++ для розв‘язання
 індивідуального завдання, що представлено у таблиці И.1 з додатку И. 
*призначення програмного файлу: У заданому масиві знайти та вивести елементи й їх індекси, що менше заданого числа.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int size, i,a; 
    cout << "Введіть розмір масиву = ";
    cin >> size;
    cout << "Введіть задане число a = ";
    cin >> a;
    srand(time(NULL));
    int a8[size];
    cout << "Введіть масив А["<<size<<"] = ";
  	for (i=0;i<size;i++)
	    {
			cin>>a8[i];
		} 
	system("cls");
	printf("Сформований масив a8[%d] = \n",size);
	for (i=0;i<size;i++)
	    {
			printf("%4d",a8[i]);
		} 
	printf("\n Елементи масиву , меньших за %d  = \n",a);
	for (i=0;i<size;i++)
	    {
			if (a8[i] < a){
				printf("%4d",a8[i]);
			}
		}
	printf("\n Індекси чисел масиву , меньших за %d = \n",a);
	for (i=0;i<size;i++)
	    {
			if (a8[i] < a){
				printf("%4d",i);
			}
		}
	cout<<endl;
	system("pause");
	return 0;
}
